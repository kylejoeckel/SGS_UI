import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Scheduled from '@/components/ScheduledTests'
import Run from '@/components/RunTests'
import Results from '@/components/TestResults'
import Configuration from '@/components/Configuration'


Vue.use(Router)

export default new Router({
  routes: [
    // {
    //   path: '/',
    //   name: 'Dashboard',
    //   component: Dashboard
    // },
    {
      path: '/',
      name: 'Run_Tests',
      component: Run
    },
    {
      path: '/results',
      name: 'Test_Results',
      component: Results
    },
    {
      path: '/scheduled',
      name: 'Scheduled Tests',
      component: Scheduled
    },
    {
      path: '/config',
      name: 'RCVR Configuration',
      component: Configuration
    }
  ],
  mode: 'history'
})
