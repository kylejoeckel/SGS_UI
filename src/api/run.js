import api from './api'

export function getTests () {
    return api.get(`/tests`)
}

export function runTest (test) {
    console.log(test)
    return api.post(`/run`, {
        test
    })
}

export function getRcvrs() {
    return api.get(`/rcvrconfig`)
}

export function saveRcvrConfig (config) {
    return api.post(`/rcvrconfig`, {
        config 
    })
}