import api from './api'

export function results () {
    return api.get(`/results`)
}
