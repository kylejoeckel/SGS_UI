import api from './api'

export function startpair (test) {
    console.log(test)
    return api.put(`/pair`, {
        test
    })
}

export function finishpair (pin, id, ip) {
    return api.post(`/pair`, {
        pin,
        id, 
        ip
    })
}
